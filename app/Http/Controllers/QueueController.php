<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\UserCounter;

class QueueController extends Controller
{
    protected function ok($message, $data, $code) {
        return response()->json([
            'message' => $message,
            'row' => $data,
        ], $code);
    }

    public function RequestQueue($id, $user_id){
        try {
            $cekantrian = UserCounter::where('counter_id',$id)->where('queue_date',Date('Y-m-d'))->get(); 

            $cekuser = UserCounter::where('counter_id',$id)->where('queue_date',Date('Y-m-d'))->where('user_id',$user_id)->where('is_processed','<>','success')->get();
            if(count($cekuser) > 0){
                $message = "failed";
                $row = 'users alerdy register !';
                $code = 400;
                return $this->ok($message, $row, $code);
            }

            $antrian = new UserCounter;
            $antrian->user_id = $user_id;
            $antrian->counter_id = $id;
            $antrian->queue_number = (count($cekantrian)+1);
            $antrian->is_processed = 'waiting';
            $antrian->queue_date = Date('Y-m-d');
            $antrian->save();

            $message = "success";
            $row = $antrian;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    public function ShowLastQueueUser($user_id){
        try {
            $cekuser = UserCounter::where('is_processed','<>','success')->where('user_id',$user_id)->where('queue_date',Date('Y-m-d'))->limit(1)->first();
            if($cekuser){
                $cekantrian = UserCounter::where('counter_id',$cekuser->counter_id)->where('queue_date',Date('Y-m-d'))->get(); 
                $est = (count($cekantrian) * 15);
                $y   = $est % 3600;
                $jam   = $est / 3600;
                $menit = $y / 60;
                $h = (Date('h') +floor($jam));
                $m = (Date('m') + floor($menit));
                $cekuser->estimate  = $h.':'.$m . Date('A');
                $cekuser->counter;
            }
            // if(count($cekuser) > 0){
                $message = "success";
                $row = $cekuser;
                $code = 200;
            // }
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    public function ListQueueHsitoryUser($user_id){
        try {
            $cekantrian = UserCounter::where('user_id',$user_id)->orderBy('updated_at','desc')->paginate(8); 

            $message = "success";
            $row = $cekantrian;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = null;
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    public function CallQueue($id){
        
        try {
            $antrian = UserCounter::where('counter_id',$id)->where('queue_date',Date('Y-m-d'))->where('is_processed','waiting')->orderBy('queue_number','Asc')->limit(1)->first();
            $antrian->is_processed = 'process';
            $antrian->save();

            $message = "success";
            $row = $antrian;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }


}
