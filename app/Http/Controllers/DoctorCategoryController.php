<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\DoctorCategory;

class DoctorCategoryController extends Controller
{
    protected function ok($message, $data, $code) {
        return response()->json([
            'message' => $message,
            'row' => $data,
        ], $code);
    }

    public function index()
    {
        try {
            $counter = DoctorCategory::paginate(10);
            
            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    public function store(Request $request){
        try {
            $counter = new DoctorCategory;
            $counter->name = $request->get('name');
            $counter->save();

            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    public function update(Request $request, $id){
        try {
            $counter = DoctorCategory::find($id);
            $counter->name = $request->get('name');
            $counter->save();

            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    public function destroy($id){
        try {
            $counter = DoctorCategory::find($id);
            $counter->delete();

            $message = "success";
            $row = $counter;
            $code = 200;
        
        } catch (Exception $err) {
            $message = $err->getMessage();
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }
}
