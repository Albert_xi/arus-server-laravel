<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \App\UserFamilyMember as Family;

class FamilyController extends Controller
{
    protected function ok($message, $data, $code) {
        return response()->json([
            'message' => $message,
            'row' => $data,
        ],$code);
    }

    public function index($user_id)
    {
        try {
            $family = Family::where('user_id',$user_id)->get();
            
            $message = "success list";
            $row = $family;
            $code = 200;
        
        } catch (Expection $err) {
            $message = "error";
            $row = $err;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function create()
    {
        
    }

    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'nik' => 'required|min:8|max:17',
            'date_of_birth' => 'date',
            'gender' => 'string',
            'place_of_birth' => 'string|min:3'
        ]);

        
        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'messages' => $validator->messages()
            ], 400);
        }
        try {
            $family = new Family;
            $family->name = $request->get('name');
            $family->user_id = $id;
            $family->nik = $request->get('nik');
            $family->gender = $request->get('gender');
            $family->date_of_birth = $request->get('date_of_birth');
            $family->place_of_birth = $request->get('place_of_birth');
            $family->save();

            $message = "success create";
            $row = $family;
            $code = 200;
        
        } catch (\Throwable $th) {
            dd($th);
            $message = "error";
            $row = $th;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function show($user_id,$id)
    {
        try {
            $family = Family::where('user_id',$user_id)->findOrFail($id);
            $message = "success show";
            $row = $family;
            $code = 200;
        } catch (Expection $err) {
            $message = "error";
            $row = $err;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $user_id, $id)
    {
        try {
            $family = Family::where('user_id', $user_id)->find($id);
            $family->name = $request->get('name');
            $family->nik = $request->get('nik');
            $family->gender = $request->get('gender');
            $family->date_of_birth = $request->get('date_of_birth');
            $family->place_of_birth = $request->get('place_of_birth');
            $family->save();
            $message = "success update";
            $row = $family;
            $code = 200;
        } catch (Expection $err) {
            $message = "error";
            $row = $err;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }

    public function destroy($user_id ,$id)
    {
        try {
            $family = Family::where('user_id', $user_id)->findOrFail($id);
            $family->delete();

            $message = "success delete";
            $row = $family;
            $code = 200;
        } catch (Expection $err) {
            $message = "error";
            $row = $err;
            $code = 400;
        }
        return $this->ok($message, $row, $code);
    }
}
