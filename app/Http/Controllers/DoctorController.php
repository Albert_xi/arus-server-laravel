<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\Doctor;
use Illuminate\Support\Facades\Validator;
use \App\Role;
use \App\DoctorCategory;

class DoctorController extends Controller
{
    protected function ok($message, $data, $code) {
        return response()->json([
            'message' => $message,
            'row' => $data,
        ],$code);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = 10;
        $sort = 'Asc';
        $order = 'name';
        $cat_id = '';
        $search = '';
        
        try {

            if($request->query('per_page') != null && strlen($request->query('per_page') > 1 )){
                $per_page = $request->query('per_page');
            }
            if($request->query('sort_by') != null && strlen($request->query('sort_by') > 1) ){
                $sort = $request->query('sort_by');
            }
            if($request->query('order_by') != null && strlen($request->query('order_by') > 0)){
                $order = $request->query('order_by');
            }
            if($request->query('search') != null){
                $search = $request->query('search');
            }
            if($request->query('category_id') != null){
                $cat_id = $request->query('category_id');
            }
            if($request->query('search_by') != null){
                $search_by = $request->query('search_by');
            }

            $doctor = User::join('doctors','doctors.user_id','=','users.id')
                        ->leftJoin('doctor_categories','doctors.doctor_category_id','=','doctor_categories.id')
                        ->select('doctors.id as id','users.name as name','users.email as email','users.phone as phone','doctors.doctor_category_id as doctor_category_id','doctor_categories.name as specialist')
                        ->orderBy($order,$sort)
                        ->where('users.name', 'like', '%'.$search.'%')
                        ->when($cat_id,function($query, $cat_id){
                            
                                $query->where('doctor_category_id', $cat_id);
                            
                        })
                        ->paginate($per_page);     
            $message = "success";
            $row = $doctor;
            $code = 200;
        
        } catch (Exception $err) {
            $message = "error";
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'email' => 'required|email|max:255|unique:users,email,NULL,id,deleted_at,NULL',
            'password' => 'required|string|min:8|max:255|confirmed',
            'password_confirmation' => 'required|string|min:8|max:255',
            'phone' => 'required|string|min:7|unique:users,phone,NULL,id,deleted_at,NULL',
            'nik' => 'integer|min:10|unique:users,nik,NULL,id,deleted_at,NULL'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'messages' => $validator->messages()
            ], 400);
        }
        try {
            $user = new User;
            $user->fill($request->all());
            $user->password = bcrypt($request->password);
            $user->save();
            $user->attachRole(3);
            
            $doctor = new Doctor;
            $doctor->user_id = $user->id;
            $doctor->doctor_category_id = $request->doctor_category_id;
            $doctor->save();

            $message = "success";
            $row = $user;
            $code = 200;
        
        } catch (Exception $err) {
            $message = "error";
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $doctor = Doctor::find($id);
            $data['doctor_category'] = DoctorCategory::select('id','name')->find($doctor->doctor_category_id);
            $data['doctor'] = User::find($doctor->user_id);

            $message = "success";
            $row = $data;
            $code = 200;
        } catch (Exception $err) {
            $message = "error";
            $row = $err->getMessage();
            $code = $err->getStatusCode();
        }
        return $this->ok($message, $row, $code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
