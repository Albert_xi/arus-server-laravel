<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model
{
    use SoftDeletes;
   
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function doctor_category()
    {
        return $this->belongsTo('App\DoctorCategory');
    }
}
