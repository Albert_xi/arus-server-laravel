@extends('layouts.master')
@section('title')
    <title>Families Management</title>
@endsection
@section('css')
<link rel="stylesheet" href="{{ url('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection
@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Families Management</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('content-body')
    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-8 offset-md-2">
            <!-- Default box -->
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Family Create</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
                </div>
                <div class="card-body ml-2 mr-2">
                    <a href="{{ url('users/'.$user->id.'/family') }}" class="btn btn-primary btn-sm mb-0"><i class="fas fa-arrow-left"></i> Family List</a>
                    <hr>
                    <form action="{{ url('users/'.$user->id.'/family') }}" method="post" class="mt-2">
                        @csrf
                        <div class="form-group">
                            <label for="inputName">Full Name</label>
                            <input name="name" type="text" placeholder="Typing user name..." id="inputName" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="inputNik">NIK</label>
                            <input name="nik" type="number" placeholder="Typing user NIK..." id="inputNik" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="inputPhone">Gender</label>
                            <div class="d-block">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="genderMale" name="gender" value="male" class="custom-control-input">
                                    <label class="custom-control-label" for="genderMale">Male</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="genderFamale" name="gender" value="famale" class="custom-control-input">
                                    <label class="custom-control-label" for="genderFamale">Famale</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPlace">Place Of Birth</label>
                            <input name="place_of_birth" type="text" placeholder="Typing place of birth..." id="inputPlace" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="inputDate">Date Of Birth</label>
                            <input name="date_of_birth" type="text" placeholder="dd/mm/yyyy" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                        </div>
                        <div class="button col-3">
                            <button type="submit" class="btn btn-block bg-gradient-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
                {{-- <div class="card-footer">
                Footer
                </div> --}}
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
@section('js')
    <script src="{{ asset('plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
    <script>
        $('[data-mask]').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    </script>
@endsection