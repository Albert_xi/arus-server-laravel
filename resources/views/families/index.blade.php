@extends('layouts.master')
@section('title')
    <title>Families Management</title>
@endsection
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Families Management</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('content-body')
    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">{{$user->name}} Family List</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
                </div>
                <div class="card-body">
                    <div class="btn-group" >
                        <a href="{{ url('users') }}" class="btn btn-group btn-primary btn-sm mt-1 mb-3"><i class="fas fa-arrow-left mt-1"></i> &nbsp;&nbsp;  Users List</a>
                        <a href="{{ url('users/'.$user->id.'/family/create') }}" class="btn btn-group btn-primary btn-sm mt-1 mb-3">Create New</a>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>NIK</th>
                                    <th>Gender</th>
                                    <th>Place, Date Of Birth</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <script>
                                    async function myDelete(id, event){
                                        if(confirm("Are You Sure to delete this")){
                                            axios.delete('/users/'+{{$user->id}}+'/family/'+id,{
                                                _token: $('input[name="_token"]').val(),
                                            })
                                            .then(()=>{
                                                location.reload();
                                            })
                                            .catch((err)=>{
                                                console.log(err);
                                            })
                                        }else{
                                            event.preventDefault();
                                        }
                                    }
                                </script>
                                @foreach ($families as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->nik}}</td>
                                        <td>{{$item->gender}}</td>
                                        <td>{{$item->place_of_birth}}, {{$item->date_of_birth}}</td>
                                        <td>
                                            <form method="post">
                                                @csrf
                                                @method('DELETE')
                                                <div class="button" align="center">
                                                    <a data-toggle="tooltip" data-placement="top" title="Edit User" href="{{url('users/'.$user->id.'/family/'.$item->id.'/edit')}}" class="btn btn-sm btn-icon btn-primary"><i class="fa fa-edit"></i></a>
                                                    <button data-toggle="tooltip" onclick="myDelete({{$item->id}})" type="button" data-placement="top" title="Delete User" class="btn btn-icon btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
                {{-- <div class="card-footer">
                Footer
                </div> --}}
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
@section('js')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(document).ready(()=>{
            
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        })
    </script>
@endsection