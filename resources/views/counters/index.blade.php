@extends('layouts.master')
@section('title')
    <title>Counter Management</title>
@endsection
@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('content-header')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Counters Management</h1>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>
@endsection
@section('content-body')
    <!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Counters List</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
                </div>
                <div class="card-body">
                    <a href="{{ url('counters/create') }}" class="btn btn-primary btn-sm mt-1 mb-3">Create New</a>
                    <hr>
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Counters</th>
                                <th>Current Queue</th>
                                <th>Last Queue</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <script>
                                async function myDelete(id, event){
                                    if(confirm("Are You Sure to delete this")){
                                        axios.delete('/counters/'+id,{
                                            _token: $('input[name="_token"]').val(),
                                        })
                                        .then(()=>{
                                            location.reload();
                                        })
                                    }else{
                                        event.preventDefault();
                                    }
                                }
                            </script>
                            @foreach ($counters as $item)
                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        @php $current = \App\UserCounter::where('counter_id',$item->id)->where('queue_date',Date('Y-m-d'))->orderBy('queue_number','desc')->select('queue_number')->limit(1)->pluck('queue_number')->first()@endphp
                                        @if($current != null) {{$current}}@else 0 @endif
                                    </td>
                                    <td>
                                        @php $last = \App\UserCounter::where('counter_id',$item->id)->where('queue_date',Date('Y-m-d'))->where('is_processed','process')->select('queue_number')->limit(1)->pluck('queue_number')->first()@endphp
                                        @if($last != null) {{$last}} @else 0 @endif
                                    </td>
                                    <td>
                                        <form method="post">
                                            @csrf
                                            @method('DELETE')
                                            <div class="button" align="center">
                                                <a data-toggle="tooltip" data-placement="top" title="Show Queue" href="{{url('counters/'.$item->id.'/queue')}}" class="btn btn-sm btn-icon btn-dark"><i class="fa fa-eye"></i></a>
                                                <a data-toggle="tooltip" data-placement="top" title="Edit Counters" href="{{url('counters/'.$item->id.'/edit')}}" class="btn btn-sm btn-icon btn-primary"><i class="fa fa-edit"></i></a>
                                                <button data-toggle="tooltip" onclick="myDelete({{$item->id}})" type="button" data-placement="top" title="Delete Counters" class="btn btn-icon btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
                {{-- <div class="card-footer">
                Footer
                </div> --}}
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
@section('js')
    <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(document).ready(()=>{
            
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        })
    </script>
@endsection