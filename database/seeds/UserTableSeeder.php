<?php

use Illuminate\Database\Seeder;
use \App\Role as Role;
use \App\User as User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        $adminRole = new Role();
        $adminRole->name = "admin";
        $adminRole->display_name = "Admin";
        $adminRole->save();

        $userRole = new Role();
        $userRole->name = "user";
        $userRole->display_name = "User";
        $userRole->save();

        $doctorRole = new Role();
        $doctorRole->name = "doctor";
        $doctorRole->display_name = "Doctor";
        $doctorRole->save();

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('rahasia123');
        $admin->phone = '088228832803';
        $admin->nik = '1234512345';
        $admin->save();
        $admin->attachRole($adminRole);

        $user = new User();
        $user->name = 'User';
        $user->email = 'user@gmail.com';
        $user->password = bcrypt('rahasia123');
        $user->phone = '088228832804';
        $user->nik = '1234512344';
        $user->save();
        $user->attachRole($userRole);

    }
}
