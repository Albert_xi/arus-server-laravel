<?php

use Illuminate\Database\Seeder;
use \App\DoctorCategory as Category;
class DoctorCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new Category();
        $user->name = 'Kandungan';
        $user->save();

        $user = new Category();
        $user->name = 'Anak';
        $user->save();

        $user = new Category();
        $user->name = 'Kulit';
        $user->save();

        $user = new Category();
        $user->name = 'Gigi';
        $user->save();

        $user = new Category();
        $user->name = 'Penyakit Dalam';
        $user->save();

        $user = new Category();
        $user->name = 'THT';
        $user->save();
    }
}
