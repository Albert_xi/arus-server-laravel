<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/', function () {
    return response()->json([
        'status' => 'success',
        'message' => 'work with api'
    ], 200);
});
Route::post('register', 'JWTAuthController@register')->name('api.jwt.register');
Route::post('login', 'JWTAuthController@login')->name('api.jwt.login');

Route::get('unauthorized', function() {
    return response()->json([
        'status' => 'error',
        'message' => 'Unauthorized'
    ], 401);
})->name('api.jwt.unauthorized');

Route::group(['middleware' => ['auth:api','role:user|admin']], function(){
    Route::get('users/{user_id}', 'UserController@show');
    Route::put('users/{user_id}', 'UserController@update');

    Route::get('users/{user_id}/family', 'FamilyController@index');
    Route::get('users/{user_id}/family/{id}', 'FamilyController@show');
    Route::post('users/{user_id}/family', 'FamilyController@store');
    Route::put('users/{user_id}/family/{id}', 'FamilyController@update');
    Route::delete('users/{user_id}/family/{id}', 'FamilyController@destroy');

    // Route::get('counters/{id}','QueueController@QueueLastAndProcess');
    Route::get('counters','CounterController@index');

    Route::post('counters/{id}/users/{user_id}','QueueController@RequestQueue');
    Route::post('counters/{id}/call','QueueController@CallQueue');
    
    Route::get('users/{user_id}/queue/last','QueueController@ShowLastQueueUser');
    Route::get('users/{user_id}/queue','QueueController@ListQueueHsitoryUser');
    
    Route::get('doctors/category', 'DoctorCategoryController@index');
    Route::get('doctors/', 'DoctorController@index');
    Route::get('doctors/{id}', 'DoctorController@show');


});

Route::group(['prefix'=>'admin','middleware'=>['auth:api','role:admin']], function(){
    Route::resource('users', 'UserController');
    Route::resource('users/{user_id}/family', 'FamilyController');
    Route::resource('counters', 'CounterController');
    Route::resource('doctors/category', 'DoctorCategoryController');
    Route::resource('doctors', 'DoctorController');
    Route::resource('doctors/schedule', 'DoctorController');
});

Route::group(['middleware' => ['auth:api']], function(){
    Route::get('refresh', 'JWTAuthController@refresh')->name('api.jwt.refresh');
    Route::get('logout', 'JWTAuthController@logout')->name('api.jwt.logout');
});

