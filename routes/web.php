<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth:web', 'role:admin']], function(){ 
    Route::get('dashboard','App\HomeController@index');
    Route::resource('users/{id}/family', 'App\FamilyController');
    Route::resource('users', 'App\UserController');
    Route::resource('counters/{id}/queue', 'App\QueueController');
    Route::resource('counters', 'App\CounterController');
    Route::resource('doctors/category', 'App\DoctorCategoryController');
    Route::resource('doctors', 'App\DoctorController');
    Route::get('logout','App\UserController@logout');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
